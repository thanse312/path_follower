# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1. Go to the Home directory
2. git clone https://LeaderFollower@bitbucket.org/MTURSE/path_follower.git
3. cd path_follower
4. cd gvsc_ws
5. cd src
6. git clone https://github.com/clearpathrobotics/LMS1xx.git
7. git clone https://github.com/jackal/jackal_simulator.git
8. git clone https://github.com/jackal/jackal.git
9. git clone https://LeaderFollower@bitbucket.org/DataspeedInc/velodyne_simulator.git
10. git clone https://github.com/ros-drivers/pointgrey_camera_driver.git
11. git clone https://github.com/jackal/jackal_desktop.git
12. cd ..
13. catkin_make

## How do I Launch the path_follower? ###

1. Open a terminal
2. cd path_follower
3. cd gvsc_ws
4. source devel/setup.bash
5. roslaunch jackal_gazebo jackal_world.launch config:=front_laser
6. Open 3 new terminals for a total of four
7. Repeat steps 2,3 and 4 for each of the new terminals
8. Use the newly created terminals to launch the following commands:
9. roslaunch jackal_navigation amcl_demo.launch
10. roslaunch jackal_viz view_robot.launch config:=localization
//need help on creating a proper ros bag? check the next section.
11. rosrun follower path_follower.py
12. Open a new terminal. At this point you should have a total of 5
13. cd path_follower
14. cd gvsc_ws
15. rosbag play 2021.~.bag

## How do I create a proper rosbag? ###
1. perform "How do I launch the path_follower?" up to 8.2
// Similar to the steps of 9 -12 of that guide
2. Open a new terminal
3. cd path_follower
4. cd gvsc_ws
5. rosbag record -a //records all topics, we technically only care about a few of these.
6. open rvis visual tool
7. place a 2d Nav goal // or do whatever you need to record.
8. when done, go ctrl-x ctrl-c the rosbag record.
9. kill the simulation the same way
10. in one of terminals that aren't doing anything, do "rosrun follower rosbag_to_waypoints.py"
11. rosbag record goal_pose 
12. rosbag play 2021.~ //the ros bag you just recorded
//what this does is convert the data you just got into something our path_follower can handle.
13. stop the rosbag recording when the rosbag that is playing is finished.
14. make a note of which ros bag is the one you just created as the proper ros bag to use.
15. you are done and all other processes can be killed.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact