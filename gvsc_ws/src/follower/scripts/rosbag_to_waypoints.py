#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
import geometry_msgs.msg
from geometry_msgs.msg import Pose, PoseStamped
from geometry_msgs.msg import PoseWithCovarianceStamped, TransformStamped

global goal_pose
goal_pose = PoseStamped()

def listener():
    rospy.Subscriber("/amcl_pose",PoseWithCovarianceStamped,callback)

def talker():

    # Publishes path to /move_base_simple/goal topic
    global path_pub
    path_pub = rospy.Publisher('/goal_pose', PoseStamped, queue_size=1)


def callback(msg):
    global goal_pose
        
    goal_pose.header.frame_id = msg.header.frame_id  
    goal_pose.header.seq = msg.header.seq 
    goal_pose.header.stamp.nsecs = msg.header.stamp.nsecs 
    goal_pose.header.stamp.secs = msg.header.stamp.secs 
    goal_pose.pose.position.x = msg.pose.pose.position.x 
    goal_pose.pose.position.y = msg.pose.pose.position.y 
    goal_pose.pose.position.z = msg.pose.pose.position.z 
    goal_pose.pose.orientation.x = msg.pose.pose.orientation.x
    goal_pose.pose.orientation.y = msg.pose.pose.orientation.y 
    goal_pose.pose.orientation.z = msg.pose.pose.orientation.z 
    goal_pose.pose.orientation.w = msg.pose.pose.orientation.w 

    path_pub.publish(goal_pose)

    rospy.loginfo("Goal Pose Set")

if __name__ == '__main__':

    # Initializes ROS node
    rospy.init_node('rosbag_to_waypoints', anonymous=True)

    talker()
    listener()

    # Sample Rate
    rate = rospy.Rate(10) # 10hz

    rospy.spin()

            

